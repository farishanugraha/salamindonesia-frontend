var files = [];
var url = {
	district: "http://kamtis.id/data/district.json",
	city: "http://kamtis.id/data/city.json",
	province: "http://kamtis.id/data/province.json",
	spot: "http://api.salamindonesia.id/v1/spot"
  // spot: "http://localhost:8000/v1/spot"
}

var CLOUDINARY_PRESET_NAME = 'salam_indonesia';
var CLOUDINARY_RETRIEVE_URL = 'https://res.cloudinary.com/does-studio/image/upload/';
var CLOUDINARY_UPLOAD_URL = 'https://api.cloudinary.com/v1_1/does-studio/image/upload';

window.URL    = window.URL || window.webkitURL;
var elBrowse  = document.getElementById("browse"),
    elPreview = document.getElementById("preview"),
    useBlob   = false && window.URL; // Set to `true` to use Blob instead of Data-URL

$(document).ready(function(e){
	getData();
	uiController();
	checkAndSubmit();
  shareController();
  bodyMovin();
});

function checkAndSubmit(){
	$('#submit_spot').attr('disabled', true);
	$('#check').on('change', function(e){
		var c = document.getElementById('check').checked;
		if(c){
			$('#submit_spot').attr('disabled', false);
			$('#submit_spot').attr('type', 'button');
		}else{
			$('#submit_spot').attr('disabled', true);
			$('#submit_spot').attr('type', 'submit');
		}
	});

	$('#submit_spot').click(function(e){
		var raw = $('#form_spot').serializeObject();
		var district = raw.district.split('/')[1];
		var city = raw.city.split('/')[1];
		var province = raw.province.split('/')[1];
		var data = {
			spot: {
				name: raw.spot_name,
				media: files,
				description: raw.description,
				address: raw.spot_address,
				additional: raw.additional
			},
			guide: {
				name: raw.name,
				phone: raw.phone,
				email: raw.email,
				address: {
					street: raw.street,
					district: district,
					city: city,
					province, province
				},
        social_media: {
          facebook: raw.facebook,
          twitter: raw.twitter,
          instagram: raw.instagram
        }
			}
		};

		$.ajax({
			method: "POST",
      crossDomain: true,
      dataType: 'json',
			url: url.spot,
			data: data,
			success: function(e){
				$('#alert').removeClass('alert-danger');
				$('#alert').addClass('alert-success');
				$('#alert').html('Data berhasil diupload. Mohon tunggu sebentar...');
				$('#alert').show('fast');
        setTimeout(function(e){
          $('#alert').html('');
          $('#alert').hide('fast');
          showImageResult(raw.name, city, province);
          // window.location.reload();
        }, 3000);
      },
      error: function(e){
        $('#alert').removeClass('alert-success');
        $('#alert').addClass('alert-danger');
        $('#alert').html('Mohon maaf, data tidak berhasil diupload.');
        $('#alert').show('fast');
        setTimeout(function(e){
          // window.location.reload();
        }, 3000);
      }
    });

	});
}

function shareController(){
  $("#shareTwitter").on("click", function(){
    openURL('https://twitter.com/intent/tweet?hashtags=salamindonesia&related=salamindonesia&text=' + encodeURIComponent('Saya telah berkontribusi informasi untuk Salam Indonesia 2018! http://salamindonesia.id'));
  });
  function openURL(url){
    window.open(url, 'Share', 'width=550, height=400, toolbar=0, scrollbars=1 ,location=0 ,statusbar=0,menubar=0, resizable=0');
  }

  $('#shareFacebook').on("click", function(e){
    var img = $('#print_this img')
    fbs_click(img[0]);
    function fbs_click(TheImg) {
      u=TheImg.src;
       // t=document.title;
      t=TheImg.getAttribute('alt');
      window.open('http://www.facebook.com/sharer.php?u='+encodeURIComponent(u)+'&t='+encodeURIComponent(t),'sharer','toolbar=0,status=0,width=626,height=436');return false;
    }
  });
}

function bodyMovin(){
  var animation = bodymovin.loadAnimation({
    container: document.getElementById('bm'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: 'src/img/anim/data.json'
  });

  animation.setSpeed(1);
}

function showImageResult(name, city, province){
  // hide form section
  $('.form-section').hide();

  // show img result
  $('.print-this').show();
  $('.result-section').show();

  $('#result_name').html(name);
  $('#result_city').html(city);
  $('#result_province').html(province);
  html2canvas(document.querySelector("#print_this")).then(canvas => {
    // show rendered html to canvas
    var imgLink = canvas.toDataURL("image/jpeg");

    // make link, download canvas as jpeg
    var a = $("<a>")
      .attr("href", imgLink)
      .attr("download", "img.jpeg")
      .appendTo("body");

    $('#download').click(function(e){
      a[0].click();
      a.remove();
    });

  });
}

function uiController(){
	$('#show_form').click(function(e){
		$('#landing_section').hide('fast');
		$('#form_section').fadeIn('fast');
	});
}

function getData(){
	$('#show_form').html('Loading...');
	$('#show_form').css('animation', 'glow .7s ease-in-out');
	$('#show_form').css('animation-iteration-count', 'infinite');

	$('#show_form').attr('disabled', true);
	$('#city').attr('disabled', true);
	$('#district').attr('disabled', true);
  $.ajax({
    method: 'GET',
    dataType: 'json',
    crossDomain: true,
    url: url.province,
    success: function(json){
      json.map((data, i)=>{
        $('#province').append('<option value="'+data.id+'/'+data.name+'">'+data.name+'</option>');
      });
      $('#show_form').html('Upload');
      $('#show_form').attr('disabled', false);
      $('#show_form').css('animation', 'none');
    },
    error: function(e){
      console.log(e)
    }
  });
	// $.getJSON(url.province, function(json){
	// 	json.map((data, i)=>{
	// 		// console.log(data)
	// 		$('#province').append('<option value="'+data.id+'/'+data.name+'">'+data.name+'</option>');
	// 	});
	// 	$('#show_form').html('Upload');
	// 	$('#show_form').attr('disabled', false);
	// 	$('#show_form').css('animation', 'none');
	// });

	$('#province').on('change', function(e){
		$('.loading-text-city').show('fast');
		var res = $(this).val().split('/');
		var id = res[0];
		var name = res[1];
    $.ajax({
      method: 'GET',
      url: url.city,
      success: function(json){
        $('#city').html('');
        json.map((data, i)=>{
          if(data.province_id == parseInt(id)){
            $('#city').append('<option value="'+data.id+'/'+data.name+'">'+data.name+'</option>');
          }
        });
        $('#city').attr('disabled', false);
        $('.loading-text-city').hide('fast');
      },
      error: function(e){
        console.log(e)
      }
    });

		// $.getJSON(url.city, function(json){
		// 	$('#city').html('');
		// 	json.map((data, i)=>{
		// 		if(data.province_id == parseInt(id)){
		// 			$('#city').append('<option value="'+data.id+'/'+data.name+'">'+data.name+'</option>');
		// 		}
		// 	});
		// 	$('#city').attr('disabled', false);
		// 	$('.loading-text-city').hide('fast');
		// });
	});

	$('#city').on('change', function(e){
		$('.loading-text-district').show('fast');
		var res = $(this).val().split('/');
		var id = res[0];
		var name = res[1];
		console.log(id, name);
    $.ajax({
      method: 'GET',
      url: url.district,
      success: function(json){
        $('#district').html('');
        json.map((data, i)=>{
          if(data.regency_id == parseInt(id)){
            $('#district').append('<option value="'+data.id+'/'+data.name+'">'+data.name+'</option>');
          }
        });
        $('#district').attr('disabled', false);
        $('.loading-text-district').hide('fast');
      },
      error: function(e){
        console.log(e)
      }
    });
		// $.getJSON(url.district, function(json){
		// 	$('#district').html('');
		// 	json.map((data, i)=>{
		// 		if(data.regency_id == parseInt(id)){
		// 			$('#district').append('<option value="'+data.id+'/'+data.name+'">'+data.name+'</option>');
		// 		}
		// 	});
		// 	$('#district').attr('disabled', false);
		// 	$('.loading-text-district').hide('fast');
		// });
	});
}

// =====
// upload image

// 2.
function readImage (file) {

  // Create a new FileReader instance
  // https://developer.mozilla.org/en/docs/Web/API/FileReader
  var reader = new FileReader();

  // Once a file is successfully readed:
  reader.addEventListener("load", function () {

    // At this point `reader.result` contains already the Base64 Data-URL
    // and we've could immediately show an image using
    // `elPreview.insertAdjacentHTML("beforeend", "<img src='"+ reader.result +"'>");`
    // But we want to get that image's width and height px values!
    // Since the File Object does not hold the size of an image
    // we need to create a new image and assign it's src, so when
    // the image is loaded we can calculate it's width and height:
    var image  = new Image();
    image.addEventListener("load", function () {

      // Concatenate our HTML image info 
      var imageInfo = file.name    +' '+ // get the value of `name` from the `file` Obj
          image.width  +'×'+ // But get the width from our `image`
          image.height +' '+
          file.type    +' '+
          Math.round(file.size/1024) +'KB';

      // Finally append our created image and the HTML info string to our `#preview` 
      document.getElementById('preview__text').innerHTML = '';
      this.title = imageInfo;
      var div = document.createElement('div');
      div.appendChild( this );
      elPreview.appendChild( div );
      // elPreview.insertAdjacentHTML("beforeend", imageInfo +'<br>');

      // If we set the variable `useBlob` to true:
      // (Data-URLs can end up being really large
      // `src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAA...........etc`
      // Blobs are usually faster and the image src will hold a shorter blob name
      // src="blob:http%3A//example.com/2a303acf-c34c-4d0a-85d4-2136eef7d723"
      if (useBlob) {
        // Free some memory for optimal performance
        window.URL.revokeObjectURL(image.src);
      }
    });
		
    image.src = useBlob ? window.URL.createObjectURL(file) : reader.result;

    var form = new FormData();

	form.append('upload_preset', CLOUDINARY_PRESET_NAME);
	form.append('file', image.src);
	
	function xhrComplete(ev) {
        var response;

        // Check the request is complete
        if (ev.target.readyState != 4) {
            return;
        }

        // Clear the request
        xhr = null
        xhrProgress = null
        xhrComplete = null

        // Handle the result of the upload
        if (parseInt(ev.target.status) == 200) {
            // Unpack the response (from JSON)
            response = JSON.parse(ev.target.responseText);

            // Store the image details
            image = {
                angle: 0,
                height: parseInt(response.height),
                maxWidth: parseInt(response.width),
                width: parseInt(response.width)
                };
                
            // Apply a draft size to the image for editing
            image.filename = response.public_id.split('/')[0] +'/'+ parseCloudinaryURL(response.url)[0];
            image.url = buildCloudinaryURL(
                image.filename,
                [{c: 'fit', h: 600, w: 600}]
                );
            
            // Populate the dialog
            files.push(image.url)

        } else {
            // The request failed, notify the user
            new ContentTools.FlashUI('no');
        }
    }

	xhr = new XMLHttpRequest();
 	xhr.upload.addEventListener('progress', function(e){
 		console.log(e)
 	});
 	xhr.addEventListener('readystatechange', xhrComplete);
	xhr.open('POST', CLOUDINARY_UPLOAD_URL, true);
 	xhr.send(form);


  });

  // https://developer.mozilla.org/en-US/docs/Web/API/FileReader/readAsDataURL
  reader.readAsDataURL(file);  
}

// 1.
// Once the user selects all the files to upload
// that will trigger a `change` event on the `#browse` input
elBrowse.addEventListener("change", function() {

  // Let's store the FileList Array into a variable:
  // https://developer.mozilla.org/en-US/docs/Web/API/FileList
  var files  = this.files;
  // Let's create an empty `errors` String to collect eventual errors into:
  var errors = "";

  if (!files) {
    errors += "File upload not supported by your browser.";
  }

  // Check for `files` (FileList) support and if contains at least one file:
  if (files && files[0]) {

    // Iterate over every File object in the FileList array
    for(var i=0; i<files.length; i++) {

      // Let's refer to the current File as a `file` variable
      // https://developer.mozilla.org/en-US/docs/Web/API/File
      var file = files[i];

      // Test the `file.name` for a valid image extension:
      // (pipe `|` delimit more image extensions)
      // The regex can also be expressed like: /\.(png|jpe?g|gif)$/i
      if ( (/\.(png|jpeg|jpg|gif)$/i).test(file.name) ) {
        // SUCCESS! It's an image!
        // Send our image `file` to our `readImage` function!
        readImage( file ); 
      } else {
        errors += file.name +" Unsupported Image extension\n";  
      }
    }
  }

  // Notify the user for any errors (i.e: try uploading a .txt file)
  if (errors) {
    alert(errors); 
  }

});

function buildCloudinaryURL(filename, transforms) {
    // Build a Cloudinary URL from a filename and the list of transforms 
    // supplied. Transforms should be specified as objects (e.g {a: 90} becomes
    // 'a_90').
    var i, name, transform, transformArgs, transformPaths, urlParts;

    // Convert the transforms to paths
    transformPaths = [];
    for  (i = 0; i < transforms.length; i++) {
        transform = transforms[i];
        
        // Convert each of the object properties to a transform argument
        transformArgs = [];
        for (name in transform) {
            if (transform.hasOwnProperty(name)) {
                transformArgs.push(name + '_' + transform[name]);
            }
        }
        
        transformPaths.push(transformArgs.join(','));
    }
    
    // Build the URL
    urlParts = [CLOUDINARY_RETRIEVE_URL];
    if (transformPaths.length > 0) {
        urlParts.push(transformPaths.join('/'));
    }
    urlParts.push(filename);

    return urlParts.join('/');
}

function parseCloudinaryURL(url) {
    // Parse a Cloudinary URL and return the filename and list of transforms
    var filename, i, j, transform, transformArgs, transforms, urlParts;

    // Strip the URL down to just the transforms, version (optional) and
    // filename.
    url = url.replace(CLOUDINARY_RETRIEVE_URL, '');

    // Split the remaining path into parts
    urlParts = url.split('/');

    // The path starts with a '/' so the first part will be empty and can be
    // discarded.
    urlParts.shift();

    // Extract the filename
    filename = urlParts.pop();

    // Strip any version number from the URL
    if (urlParts.length > 0 && urlParts[urlParts.length - 1].match(/v\d+/)) {
        urlParts.pop();
    }

    // Convert the remaining parts into transforms (e.g `w_90,h_90,c_fit >
    // {w: 90, h: 90, c: 'fit'}`).
    transforms = [];
    for (i = 0; i < urlParts.length; i++) {
        transformArgs = urlParts[i].split(',');
        transform = {};
        for (j = 0; j < transformArgs.length; j++) {
            transform[transformArgs[j].split('_')[0]] =
                transformArgs[j].split('_')[1];
        }
        transforms.push(transform);
    }

    // console.log(filename, transforms)
    return [filename, transforms];
}

// =================================
//fungsi baru untuk bikin json parse
(function($){
    $.fn.serializeObject = function(){
        var self = this,
            json = {},
            push_counters = {},
            patterns = {
                "validate": /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
                "key":      /[a-zA-Z0-9_]+|(?=\[\])/g,
                "push":     /^$/,
                "fixed":    /^\d+$/,
                "named":    /^[a-zA-Z0-9_]+$/
            };
        this.build = function(base, key, value){
            base[key] = value;
            return base;
        };
        this.push_counter = function(key){
            if(push_counters[key] === undefined){
                push_counters[key] = 0;
            }
            return push_counters[key]++;
        };
        $.each($(this).serializeArray(), function(){
            // skip invalid keys
            if(!patterns.validate.test(this.name)){
                return;
            }
            var k,
                keys = this.name.match(patterns.key),
                merge = this.value,
                reverse_key = this.name;
            while((k = keys.pop()) !== undefined){
                // adjust reverse_key
                reverse_key = reverse_key.replace(new RegExp("\\[" + k + "\\]$"), '');
                // push
                if(k.match(patterns.push)){
                    merge = self.build([], self.push_counter(reverse_key), merge);
                }
                // fixed
                else if(k.match(patterns.fixed)){
                    merge = self.build([], k, merge);
                }
                // named
                else if(k.match(patterns.named)){
                    merge = self.build({}, k, merge);
                }
            }
            json = $.extend(true, json, merge);
        });
        return json;
    };
})(jQuery);
//end function
